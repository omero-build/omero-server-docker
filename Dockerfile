FROM manics/omero-server:noserver as unzip
ADD OMERO.server.zip /opt/omero/server/
RUN cd /opt/omero/server/ && \
    unzip -q OMERO.server.zip && \
    mv OMERO.server-* OMERO.server


######################################################################
# Multi-stage build so OMERO.server.zip isn't bundled
FROM manics/omero-server:noserver
MAINTAINER ome-devel@lists.openmicroscopy.org.uk

USER root

# OMERO.py plugins
RUN pip install \
    omero-cli-render \
    omero-metadata

USER omero-server

COPY --from=unzip --chown=omero-server /opt/omero/server/OMERO.server /opt/omero/server/OMERO.server

EXPOSE 4064
